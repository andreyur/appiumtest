# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
Simple mobile test automation framework example
* Version
1.0

### How do I get set up? ###

* Summary of set up
You should have JDK, SDK, Appium
* Configuration
If you want to use test with your device you should have Colorograf application, 
and your device number(like 9PTOD685PF7LMBSO. You can find it using command "adb devices")
* Dependencies
All required dependencies are include to pom.xml
* How to run tests
Just right mouse click-> Run test

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Other community or team contact
People who just start their automation career
package com.hillel.android;

import android.Screens.DiscoverScreen;
import android.Screens.LoginScreen;
import android.Screens.MyRecipesScreen;
import android.Screens.SettingsScreen;
import Utils.DriverFactory;
import io.appium.java_client.android.AndroidDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;

public class AndroidBaseTest {
    private static AndroidDriver driver;
    LoginScreen loginScreen;
    DiscoverScreen discoverScreen;

    @BeforeTest
    public void setUp() {
        driver = (AndroidDriver) DriverFactory.getDriver("Android");
    }

    @BeforeMethod
    public void beforeMethod() {
        loginScreen = new LoginScreen(driver);
        discoverScreen = new DiscoverScreen(driver);
        if (loginScreen.isOpened()) {
            loginScreen.clickToMaybeLaterButton();
            if (loginScreen.isNewsletterPopUpOpened()) loginScreen.clickToDismissNewsletterButton();
            loginScreen.clickToOnboardingNotVeganButton();
        }
    }

    @Test(dataProvider = "Autoplay")
    public void autoplayVideosTest(String mode, boolean autoplayOn) {
        MyRecipesScreen myRecipesScreen = discoverScreen.clickToMyRecipesButton();
        SettingsScreen settingsScreen = myRecipesScreen.clickToSettingsButton();
        settingsScreen.clickToAutoplayVideosButton(mode);
        settingsScreen.clickToBackButton();
        myRecipesScreen.clickToDiscoverButton();
        assertEquals(discoverScreen.isAutoplayOn(), autoplayOn);
        discoverScreen.clickToBackButton();
    }

    @DataProvider(name = "Autoplay")
    public static Object[][] search() {
        return new Object[][]{
                {"Always", true},
                {"Wifi", true},
                {"Never", false}
        };
    }

    @AfterMethod
    public void afterMethod(ITestResult result){
        discoverScreen.recordFailure(result);
    }

    @AfterTest
    public void afterTest() {
        //Close the browser
        driver.quit();
    }

}
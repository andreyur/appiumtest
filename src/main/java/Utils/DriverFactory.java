package Utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.options.XCUITestOptions;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

import java.io.File;
import java.time.Duration;
import static io.appium.java_client.service.local.AppiumDriverLocalService.buildService;

public class DriverFactory {
    public static AppiumDriver getDriver (String name) {
        String deviceName;
        File app;
        AppiumDriverLocalService service;
        UiAutomator2Options options;
        switch (name) {
            case "iOS":
                app = new File("src/main/resources/QARI_v11.2.22.app");
                deviceName = "iPhone 13 mini";
                service = buildService(new AppiumServiceBuilder().usingAnyFreePort());
                service.start();
                XCUITestOptions iOSOptions = new XCUITestOptions()
                        .setDeviceName(deviceName)
                        .setApp(app.getAbsolutePath())
                        .setPlatformName("iOS")
                        .setPlatformVersion("17.2")
                        .setBundleId("com.nn4m.riverisland.qa")
                        .setFullReset(false)
                        .setClearSystemFiles(false)
                        .setNoReset(true)
                        .setNewCommandTimeout(Duration.ofMillis(600));

                return new IOSDriver(service.getUrl(), iOSOptions);
            case "Android":
            default:
                app = new File("src/main/resources/com.buzzfeed.tasty.apk");
                deviceName = "Android Emulator";
                service = buildService(new AppiumServiceBuilder().usingAnyFreePort());
                service.start();
                options = new UiAutomator2Options()
                        .setDeviceName(deviceName)
                        .setApp(app.getAbsolutePath())
                        .setPlatformName("Android")
                        .setAvd("Pixel_6_API_33")
                        .setAppPackage("com.buzzfeed.tasty")
                        .setAppActivity("LauncherActivity")
                        .setFullReset(false)
                        .setClearSystemFiles(false)
                        .setNoReset(true)
                        .setNewCommandTimeout(Duration.ofMillis(600));

                return new AndroidDriver(service.getUrl(), options);
        }
    }
}

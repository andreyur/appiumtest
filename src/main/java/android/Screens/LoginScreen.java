package android.Screens;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginScreen extends BaseScreen {

    public LoginScreen(AndroidDriver driver) {
        super(driver);
    }

    public static final String MAYBE_LATER_BUTTON = "//android.widget.TextView[@text='Maybe later']";
    public static final String ONBOARDING_VEGAN_BUTTON = "com.buzzfeed.tasty:id/yesButton";
    public static final String ONBOARDING_NOT_VEGAN_BUTTON = "com.buzzfeed.tasty:id/noButton";
    public static final String DISMISS_NEWSLETTER_BUTTON = "com.buzzfeed.tasty:id/dismiss";

    @FindBy(xpath = MAYBE_LATER_BUTTON)
    private WebElement maybeLaterButton;

    public WebElement getMaybeLaterButton() {
        return maybeLaterButton;
    }

    @FindBy(id = ONBOARDING_VEGAN_BUTTON)
    private WebElement onboardingVeganButton;

    public WebElement getOnboardingVeganButton() {
        return onboardingVeganButton;
    }

    @FindBy(id = ONBOARDING_NOT_VEGAN_BUTTON)
    private WebElement onboardingNotVeganButton;

    public WebElement getOnboardingNotVeganButton() {
        return onboardingNotVeganButton;
    }

    @FindBy(id = DISMISS_NEWSLETTER_BUTTON)
    private WebElement dismissNewsletterButton;

    public WebElement getDismissNewsletterButton() {
        return dismissNewsletterButton;
    }

    public void clickToMaybeLaterButton() {
        maybeLaterButton.click();
    }

    public void clickToOnboardingVeganButton() {
        onboardingVeganButton.click();
    }

    public void clickToOnboardingNotVeganButton() {
        onboardingNotVeganButton.click();
    }

    public void clickToDismissNewsletterButton() {
        dismissNewsletterButton.click();
    }

    public boolean isOpened() {
        boolean opened = driver.findElements(By.xpath(MAYBE_LATER_BUTTON)).size() != 0;
        return opened;
    }

    public boolean isNewsletterPopUpOpened() {
        boolean opened = driver.findElements(By.id(DISMISS_NEWSLETTER_BUTTON)).size() != 0;
        return opened;
    }
}

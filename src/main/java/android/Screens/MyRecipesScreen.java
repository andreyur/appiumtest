package android.Screens;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyRecipesScreen extends BaseScreen {
    public MyRecipesScreen(AndroidDriver driver) {
        super(driver);
    }

    public static final String DISCOVER_BUTTON = "com.buzzfeed.tasty:id/action_discover";
    public static final String SETTINGS_BUTTON = "com.buzzfeed.tasty:id/menu_settings";

    @FindBy(id = DISCOVER_BUTTON)
    private WebElement discoverButton;

    public WebElement getDiscoverButton() {
        return discoverButton;
    }

    @FindBy(id = SETTINGS_BUTTON)
    private WebElement settingsButton;

    public WebElement getSettingsButton() {
        return settingsButton;
    }

    public DiscoverScreen clickToDiscoverButton() {
        discoverButton.click();
        return new DiscoverScreen(driver);
    }

    public SettingsScreen clickToSettingsButton() {
        settingsButton.click();
        return new SettingsScreen(driver);
    }
}

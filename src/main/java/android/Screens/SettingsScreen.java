package android.Screens;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SettingsScreen extends BaseScreen {
    public SettingsScreen(AndroidDriver driver) {
        super(driver);
    }

    public static final String AUTOPLAY_VIDEOS_BUTTON = "//android.widget.TextView[@text='Autoplay Videos']";
    public static final String DIETARY_RESTRICTIONS_BUTTON = "//android.widget.TextView[@text='Dietary Restrictions']";
    public static final String AUTOPLAY_ALWAYS_BUTTON = "//android.widget.RadioButton[@text='Always']";
    public static final String AUTOPLAY_WIFI_BUTTON = "//android.widget.RadioButton[@text='On Wifi Only']";
    public static final String AUTOPLAY_NEVER_BUTTON = "//android.widget.RadioButton[@text='Never']";
    public static final String DONE_BUTTON = "//android.widget.TextView[@text='DONE']";
    public static final String BACK_BUTTON = "//android.widget.ImageButton[@content-desc='Navigate up']";

    @FindBy(xpath = AUTOPLAY_VIDEOS_BUTTON)
    private WebElement autoplayVideosButton;

    public WebElement getAutoplayVideosButton() {
        return autoplayVideosButton;
    }

    @FindBy(xpath = AUTOPLAY_ALWAYS_BUTTON)
    private WebElement autoplayAlwaysButton;

    public WebElement getAutoplayAlwaysButton() {
        return autoplayAlwaysButton;
    }

    @FindBy(xpath = AUTOPLAY_WIFI_BUTTON)
    private WebElement autoplayWifiButton;

    public WebElement getAutoplayWifiButton() {
        return autoplayWifiButton;
    }

    @FindBy(xpath = AUTOPLAY_NEVER_BUTTON)
    private WebElement autoplayNeverButton;

    public WebElement getAutoplayNeverButton() {
        return autoplayNeverButton;
    }

    @FindBy(xpath = DIETARY_RESTRICTIONS_BUTTON)
    private WebElement dietaryRestrictionsButton;

    public WebElement getDietaryRestrictionsButton() {
        return dietaryRestrictionsButton;
    }

    @FindBy(xpath = DONE_BUTTON)
    private WebElement doneButton;

    public WebElement getDoneButton() {
        return doneButton;
    }

    @FindBy(xpath = BACK_BUTTON)
    private WebElement backButton;

    public WebElement getBackButton() {
        return backButton;
    }

    public void clickToAutoplayVideosButton(String mode) {
        autoplayVideosButton.click();
        switch (mode) {
            case "Always":
                autoplayAlwaysButton.click();
                break;
            case "Wifi":
                autoplayWifiButton.click();
                break;
            case "Never":
                autoplayNeverButton.click();
                break;
        }
        doneButton.click();
    }

    public void clickToDietaryRestrictionsButton() {
        dietaryRestrictionsButton.click();
    }

    public void clickToBackButton() {
        backButton.click();
    }
}

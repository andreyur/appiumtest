package android.Screens;


import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Random;

public class DiscoverScreen extends BaseScreen {

    public DiscoverScreen(AndroidDriver driver) {
        super(driver);
    }

    public static final String RECIPE_IMAGE = "com.buzzfeed.tasty:id/thumbnail";
    public static final String MY_RECIPES_BUTTON = "com.buzzfeed.tasty:id/action_my_recipes";
    public static final String PLAY_BUTTON = "com.buzzfeed.tasty:id/initialPlayButton";
    public static final String BACK_BUTTON = "//android.widget.ImageButton[@content-desc='Navigate up']";

    @FindBy(id = RECIPE_IMAGE)
    private List<WebElement> recipeImage;

    public List<WebElement> getRecipeImage() {
        return recipeImage;
    }

    @FindBy(id = MY_RECIPES_BUTTON)
    private WebElement myRecipesButton;

    public WebElement getMyRecipesButton() {
        return myRecipesButton;
    }

    @FindBy(id = PLAY_BUTTON)
    private WebElement playButton;

    public WebElement getPlayButton() {
        return playButton;
    }

    @FindBy(xpath = BACK_BUTTON)
    private WebElement backButton;

    public WebElement getBackButton() {
        return backButton;
    }

    public void clickToRecipeImage(int number) {
        recipeImage.get(number).click();
    }

    public MyRecipesScreen clickToMyRecipesButton() {
        myRecipesButton.click();
        return new MyRecipesScreen(driver);
    }

    public void clickToRandomRecipeImage() {
        recipeImage.get(new Random().nextInt(getRecipeImage().size())).click();
    }

    public void clickToBackButton() {
        backButton.click();
    }

    public boolean isAutoplayOn() {
        clickToRandomRecipeImage();
        boolean autoplayOn = driver.findElements(By.id(PLAY_BUTTON)).size() == 0;

        return autoplayOn;
    }
}

package android.Screens;


import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class BaseScreen {
    protected AndroidDriver driver;

    public BaseScreen(AndroidDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        PageFactory.initElements(driver, this);
    }

    public void recordFailure(ITestResult result){
        if(ITestResult.FAILURE == result.getStatus())
       {
            String methodName=result.getName().toString().trim();
            TakesScreenshot camera = (TakesScreenshot)driver;
            File screenshot = camera.getScreenshotAs(OutputType.FILE);
           String dateFormat = "dd-MM-yyyy'-'HH-mm-ss";
           SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
           Date date = new Date(result.getEndMillis());
            try{
                FileUtils.copyFile(screenshot, new File("target/screenshots/" + result.getName() + "-" + simpleDateFormat.format(date)+".png"));
            }catch(IOException e){
                e.printStackTrace();
            }
           takeScreenshot();
       }
    }

    @Attachment(value = "Screenshot", type = "image/png")
    public byte[] takeScreenshot() {
        // Take a screenshot as byte array and return
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    }
